import pandas as pd
from download_nomenclatures import get_filetype_from_tree
import json

def selection_variables_ngram(folder):

    variables={}

    nomenclatures_csv = get_filetype_from_tree(folder, extension='.csv')
    nomenclatures_json = get_filetype_from_tree(folder, extension='.json')

    for nomenclature in nomenclatures_csv:
        data_temp = pd.read_csv(nomenclature, sep=';', nrows=1)
        nomenclature_name = nomenclature.split('/')[-1].split('.')[0]
        variables[nomenclature_name] = list(data_temp.columns)

    for key, val in variables.items():
        variables[key] = [var for var in val if var[-3:] == 'LIB']

    for nomenclature in nomenclatures_json:
        with open(nomenclature) as f:
            temp_json = json.load(f)
        for field in temp_json['fields']:
            if ('role' in field.keys()) and (field['role'] == 'label'):
                if ('name' in field.keys()) and field['name'] not in variables[temp_json['name']]:
                    variables[temp_json['name']].append(field['name'])

    with open('variables_ngram.json', 'w') as outfile:
        json.dump(variables, outfile)

if __name__=='__main__':

    untarname = 'schema-snds-master-nomenclatures'
    selection_variables_ngram(untarname)