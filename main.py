#!/usr/bin/env python
#-*- coding: utf-8 -*-

import argparse

import pandas as pd
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from elasticsearch import client
from getpass import getpass
import json
from download_nomenclatures import dl_from_gitlab, get_filetype_from_tree
from bulk_actions_maker import df_to_doc_generator
from variables_selection import selection_variables_ngram

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--elasticsearch", help="adresse du serveur elasticsearch")
    parser.add_argument("-nb", "--nombre_index", type=int, help="lance le code que sur un nombre limité d'index")
    parser.add_argument('-cln', '--clean_indices',
                        help="effacer les indices nomenclature existant", action='store_true')
    parser.add_argument('-v', '--verbose',
                        help="Afficher un reporting nomenclature par nomenclature", action='store_true')
    parser.add_argument('-d', '--download',
                        help="Télécharger les nomenclatures depuis le gitlab", action='store_true')
    args = parser.parse_args()

    # On télécharge les nomenclatures

    url = 'https://gitlab.com/healthdatahub/schema-snds/-/archive/master/schema-snds-master.tar.gz?path=nomenclatures'
    tarname = 'nomenclatures.tar.gz'
    untarname = 'schema-snds-master-nomenclatures'

    # On demande les logins

    username = input('Enter username ')
    password = getpass('Enter password ')

    # Mise a jour des nomenclatures

    if args.download:
        dl_from_gitlab(url, tarname, untarname)
    
    nomenclatures = get_filetype_from_tree(untarname, extension='.csv')
    if args.nombre_index:
        nomenclatures = nomenclatures[:args.nombre_index]

    # Preparation du json pour la selection des variables

    selection_variables_ngram(untarname)
    with open('variables_ngram.json') as json_file:
        variables_ngram = json.load(json_file)
    # Quelques modifications a la main
    variables_ngram['IR_MTR_V'] = ['TRS_MTF_LB1', 'TRS_MTF_LB2', 'TRS_MTF_LB3']

    # Connexion à ES
    if args.elasticsearch:
        es = Elasticsearch([args.elasticsearch], http_auth=(username, password))
    else:
        es = Elasticsearch([{'host': 'localhost', 'port': 9200}], http_auth=(username, password))

    ic = client.IndicesClient(es)

    # On clean les indices ayant nomenclature en alias si demandé
    if args.clean_indices:
        if es.indices.exists_alias('nomenclature'):
            for e in list(es.indices.get('nomenclature').keys()):
                ic.delete(e)
            print('------Indices nettoyés------')

    # On charge le modèle de l'analyzer
    with open('my_analyzer.json') as json_file:
        my_analyzer = json.load(json_file)

    # On itère sur les nomenclatures à indexer
    # Le nom de l'index est le nom du csv en minuscule
    for nomenclature in nomenclatures:
        temp_df = pd.read_csv(nomenclature, sep=';')
        temp_size = temp_df.shape[0]
        temp_csv_name = nomenclature.split('/')[-1]
        temp_index_name = temp_csv_name.split('.')[0]

        ic.create(index=temp_index_name.lower(), body=my_analyzer)
        es.indices.update_aliases({
            "actions": [{"add": {"index": temp_index_name.lower(), "alias": "nomenclature"}}]})

        for variable in variables_ngram[temp_index_name]:
            ic.put_mapping(index=temp_index_name.lower(), 
                            body={"properties": {variable: {
                                                        "type":"text",
                                                        "analyzer": "substring",
                                                        "search_analyzer": "icu_analyzer"
                                                        }
                                            }
                                })

        resp = helpers.bulk(es, df_to_doc_generator(temp_df, temp_index_name.lower()), raise_on_error=False)
        if args.verbose:
            print(temp_index_name + ': {} sur {} lignes indexées'.format(resp[0], temp_size))
        # Tous les index ont comme alias nomenclature ce qui rend les traitements
        # plus faciles
