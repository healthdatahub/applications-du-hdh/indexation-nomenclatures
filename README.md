# Indexation des nomenclatures dans Elasticsearch 



## Installation

**Dépendances** : python3, Docker, Docker-Compose

Création d'un environnement virtuel Python avec les dépendances installées

    ./create_vitualenv.sh


Démarrage d'ElasticSearch et Kibana

     docker-compose up -d

Indexation des nomenclatures dans ElasticSearch

    ./main.py nomenclatures


Ouvrir Kibana sur [http://localhost:5601](http://localhost:5601).

Paramétrer les index pattern 
- [Onglet](http://localhost:5601/app/kibana#/management/kibana/index_pattern) Management (le dernier) > Kibana > Index Pattern
- Bouton "Create index pattern"
- Remplir case avec  "nomenclature"
- "Next step" puis valider la création

Tester la recherche dans l'onglet [Discover](http://localhost:5601/app/kibana#/discover).

