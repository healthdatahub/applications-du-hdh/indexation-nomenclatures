#!/usr/bin/env bash
set -eo pipefail

# Lancement de ES et Kibana

# Dossier pour le volume des données ES
mkdir -p data_ES

docker-compose up -d 

while ! docker exec -t -i elasticsearch /usr/share/elasticsearch/bin/elasticsearch-setup-passwords interactive -u "http://localhost:9200"
do
	echo -e "\nWaiting for Elasticsearch to be ready, it might take 1 or 2 minutes."
	sleep 20
done

# Creation de l'environnement python
pip3 install virtualenv
virtualenv venv --python=python3
source venv/bin/activate
pip install -r requirements.txt

python main.py nomenclatures